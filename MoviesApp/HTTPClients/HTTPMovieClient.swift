//
//  HTTPMovieClient.swift
//  MoviesApp
//
//  Created by Greg Hughes on 3/25/21.
//  Copyright © 2021 Mohammad Azam. All rights reserved.
//

import Foundation

class HTTPMovieClient: ObservableObject {
    
    @Published var movies: [Movie] = []
    @Published var reviews: [Review] = []
    
    func getReviewsByMovie(movie: Movie) {
        guard let uuidString = movie.id?.uuidString,
              let url = URL(string: "http://localhost:8080/movies/\(uuidString)/reviews")
        else {print("❇️♊️>>>\(#file) \(#line): guard let failed<<<"); fatalError("URL ISNT DEFINED")}
        URLSession.shared.dataTask(with: url) { (data, _, error) in
            guard let data = data, error == nil else {print("❇️♊️>>>\(#file) \(#line): guard let failed<<<"); return}
            
            let decodedReviews = try? JSONDecoder().decode([Review].self, from: data)
            if let decodedReviews = decodedReviews {
                DispatchQueue.main.async {
                    self.reviews = decodedReviews
                }
            }
            
            
        }.resume()
    }
    
    
    func deleteMovie(movie: Movie, completion: @escaping (Bool) -> Void) {
        guard let uuidString = movie.id?.uuidString,
              let url = URL(string: "http://localhost:8080/movies/\(uuidString)")
        else {print("❇️♊️>>>\(#file) \(#line): guard let failed<<<"); fatalError("URL ISNT DEFINED")}
        
        var request = URLRequest(url: url)
        request.httpMethod = "DELETE"
        
        URLSession.shared.dataTask(with: request) { (data, _, error) in
            guard let _ = data, error == nil else {print("❇️♊️>>>\(#file) \(#line): guard let failed<<<"); return completion(false)}
            completion(true)
        }.resume()
    }
    
    
    func fetchAllMovies() {
        guard let url = URL(string: "http://localhost:8080/movies") else {print("❇️♊️>>>\(#file) \(#line): guard let failed<<<"); fatalError("URL ISNT DEFINED")}
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data, error == nil else {print("❇️♊️>>>\(#file) \(#line): guard let failed<<<"); return}
            do {
                let movies = try JSONDecoder().decode([Movie].self, from: data)
                    DispatchQueue.main.async {
                        self.movies = movies
                    }
                
            }catch let error {
                print("🌫 \(error)")
            }
            
        }.resume()
        
        
    }
    
    func saveMovie(name: String, poster: String, completion:@escaping (Bool) -> Void) {
        guard let url = URL(string: "http://localhost:8080/movies") else {print("❇️♊️>>>\(#file) \(#line): guard let failed<<<"); fatalError("URL ISNT DEFINED")}
        
        let movie = Movie(title: name, poster: poster)
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = try? JSONEncoder().encode(movie)
        
        URLSession.shared.dataTask(with: request) { (data, res, error) in
            guard let _ = data, error == nil else {print("❇️♊️>>>\(#file) \(#line): guard let failed<<<"); return completion(false)}
            
            completion(true)
            
        }.resume()
    }
    
    func saveReview(review: Review, completion:@escaping (Bool) -> Void) {
        guard let url = URL(string: "http://localhost:8080/reviews") else {print("❇️♊️>>>\(#file) \(#line): guard let failed<<<"); fatalError("URL ISNT DEFINED")}
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.httpBody = try? JSONEncoder().encode(review)
        
        URLSession.shared.dataTask(with: request) { (data, res, error) in
            guard let _ = data, error == nil else {print("❇️♊️>>>\(#file) \(#line): guard let failed<<<"); return completion(false)}
            
            completion(true)
            
        }.resume()
    }
}
