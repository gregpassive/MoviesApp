//
//  Movie.swift
//  MoviesApp
//
//  Created by Greg Hughes on 3/25/21.
//  Copyright © 2021 Mohammad Azam. All rights reserved.
//

import Foundation

struct Movie: Codable {
//    internal init(id: UUID? = nil, title: String, poster: String) {
//        self.id = id
//        self.title = title
//        self.poster = poster
//    }
    
    var id: UUID?
    var title: String
    var poster: String?
    
    private enum MovieKeys: String, CodingKey {
        case id
        case title
        case poster
    }
}

extension Movie {
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: MovieKeys.self)
        self.id = try container.decode(UUID.self, forKey: .id)
        self.title = try container.decode(String.self, forKey: .title)
        self.poster = try container.decode(String?.self, forKey: .poster)
    }
}
