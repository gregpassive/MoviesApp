//
//  MovieDetailsView.swift
//  MoviesApp
//
//  Created by Mohammad Azam on 6/19/20.
//  Copyright © 2020 Mohammad Azam. All rights reserved.
//

import SwiftUI

struct MovieDetailsView: View {
    
    let movie: Movie
    
    @Environment(\.presentationMode) private var presentationMode
    @State private var reviewTitle: String = ""
    @State private var reviewBody: String = ""
    
    @ObservedObject private var httpClient = HTTPMovieClient()
    
    private func deleteMovie() {
        HTTPMovieClient().deleteMovie(movie: movie) { (success) in
            if success {
                DispatchQueue.main.async {
                    self.presentationMode.wrappedValue.dismiss()
                }
            }
        }
    }
    private func saveReview() {
        let review = Review( title: self.reviewTitle, body: self.reviewBody, movie: movie)
        HTTPMovieClient().saveReview(review: review) { success in
            if success {
                self.httpClient.getReviewsByMovie(movie: movie)
                reviewTitle = ""
                reviewBody = ""
            }
        }
    }
    var body: some View {
        
        Form {
            
            Image(movie.poster ?? "placeholder")
                .resizable()
                .aspectRatio(contentMode: .fit)
                .padding()
            
            Section(header: Text("ADD A REVIEW").fontWeight(.bold)) {
                VStack(alignment: .center, spacing: 10) {
                    TextField("Enter Title",text: $reviewTitle)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                    
                    TextField("Enter Body",text: $reviewBody) .textFieldStyle(RoundedBorderTextFieldStyle())
                    
                    Button("Save") {
                        self.saveReview()
                        
                    }
                    .frame(maxWidth: .infinity)
                    .padding()
                    .foregroundColor(Color.white)
                    .background(Color.blue)
                    .font(.system(size: 25))
                    .cornerRadius(10)
                    .buttonStyle(PlainButtonStyle())
                }
                
            }.onTapGesture {
//                print("save")
//                self.saveReview()
            }
            
            Section(header: Text("REVIEWS").fontWeight(.bold)) {
                ForEach(self.httpClient.reviews, id: \.id ) { review in
                    Text(review.title)
                }
            }
        }
        .onAppear {
            httpClient.getReviewsByMovie(movie: movie)
        }
        .navigationBarTitle(movie.title)
            
        .navigationBarItems(trailing: Button(action: {
            self.deleteMovie()
        }) {
            Image(systemName: "trash.fill")
        })
    }
}

struct MovieDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        MovieDetailsView(movie: Movie(title: "Birds of Prey", poster: "birds"))
        
    }
}
